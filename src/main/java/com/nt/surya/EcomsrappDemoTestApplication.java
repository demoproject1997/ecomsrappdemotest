package com.nt.surya;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EcomsrappDemoTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(EcomsrappDemoTestApplication.class, args);
	}

}
